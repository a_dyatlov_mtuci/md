/**
 * Класс трёхмерной точки, унаследованный от двумерной точки 
 */
public class Point3d extends Point2d {
    /**
     * Координата Z точки
     */
    protected double zCoord;

    /**
     * Конструктор инициализации объекта класса Point3d
     */
    public Point3d(double x, double y, double z) {
        xCoord = x;
        yCoord = y;
        zCoord = z;
    }
    
    /**
     * Конструктор по умолчанию
     */
    public Point3d() {
        this(0, 0, 0);
    }

    /**
     * Возврат координаты Z
     */
    public double getZ() {
        return zCoord;
    }

    /**
     * Установка координаты Z
     */
    public void setZ(double z) {
        zCoord = z;
    }

    /**
     * Переопределение метода сравнения equals
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        } 
        if (!(obj instanceof Point3d)) {
            return false;
        }
        Point3d point2 = (Point3d) obj;
        return (xCoord == point2.xCoord && 
                yCoord == point2.yCoord &&
                zCoord == point2.zCoord);
    }

    /**
     * Метод расчёта расстояния до трёхмерной точки 
     * результат с точностью до 2-го знака после запятой
     * @param toPoint
     * @return
     */
    public double distanceTo(Point3d toPoint) {
        double distance = Math.sqrt(Math.pow((xCoord - toPoint.xCoord), 2) + 
                        Math.pow((yCoord - toPoint.yCoord), 2) +
                        Math.pow((zCoord - toPoint.zCoord), 2));
        return Math.round(distance * Math.pow(10, 2)) / Math.pow(10, 2);
    }
}