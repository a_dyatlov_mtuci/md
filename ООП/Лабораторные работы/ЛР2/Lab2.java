public class Lab2 {
    public static void main(String[] args) {
        Point3d[] triangle = new Point3d[3];
        double[] coords = new double[args.length];
        if (args.length < 9) {
            errorMsg();
        }
        
        for(int i = 0; i < args.length; i++) {
              try {
                coords[i] = Double.parseDouble(args[i]);                   
              }
              catch (Throwable e) {
                errorMsg();
              }
        }

        for(int j = 0; j < triangle.length; j++) {
          triangle[j] = new Point3d(coords[2 * j + j], coords[2 * j + 1 + j], coords[2 * j + 2 + j]);
        }

        System.out.printf("Площадь треугольника: %10.5f\n", 
                          computeArea(triangle[0], triangle[1], triangle[2]));
    }

    public static double computeArea(Point3d p1, Point3d p2, Point3d p3) {
      if(p1.equals(p2) || p2.equals(p3) || p1.equals(p3)) {
        errorMsg();
      }
      
      double d_1 = p1.distanceTo(p2);
      double d_2 = p2.distanceTo(p3);
      double d_3 = p1.distanceTo(p3);
      double pp = (d_1 + d_2 + d_3) / 2;
      return Math.sqrt(pp * (pp - d_1) * (pp - d_2) * (pp - d_3));
    }

    /**
     * Вывод сообщения об ошибке и завершение программы
     */
    private static void errorMsg() {
        System.out.println("Ошибка: Неверно заданы координаты точек треугольника!");
        System.exit(1);
    }
}