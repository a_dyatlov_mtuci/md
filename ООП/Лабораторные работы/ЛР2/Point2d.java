/** 
 * Класс двумерной точки 
 */
public class Point2d {
    
    /**
     * Координата X точки
     */
    protected double xCoord;
    
    /**
     * Координата Y точки
     */
    protected double yCoord;

    /**
     * Конструктор инициализации объекта класса Point2d
     */
    public Point2d(double x, double y) {
        xCoord = x;
        yCoord = y;
    }

    /**
     * Конструктор по умолчанию
     */
    public Point2d() {
        this(0, 0);
    }

    /**
     * Возврат координаты X
     */
    public double getX() {
        return xCoord;
    }

    /**
     * Возврат координаты Y
     */
    public double getY() {
        return yCoord;
    }

    /**
     * Установка координаты X
     */
    public void setX(double x) {
        xCoord = x;
    }
    
    /**
     * Установка координаты Y
     */
    public void setY(double y) {
        yCoord = y;
    }

    /**
     * Переопределение метода сравнения equals
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        } 
        if (!(obj instanceof Point2d)) {
            return false;
        }
        Point2d point2 = (Point2d) obj;
        return (xCoord == point2.xCoord && yCoord == point2.yCoord);
    }
}