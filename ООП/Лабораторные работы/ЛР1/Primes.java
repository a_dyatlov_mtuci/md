import java.util.ArrayList;
import java.util.List;
/**
 * Главный класс программы Primes
 */ 
class Primes {
    public static void main(String[] args) {
        int[] primes;
        int n;
        try {
            n = Integer.parseInt(args[0]);
        } catch (Throwable e) {
            n = 100;
        }
        primes = solution(n);
        System.out.print("Primes:\n");
        for(int prime: primes) {
            System.out.printf("%4d", prime);
        }
        System.out.print('\n');
    }

    /**
     * Метод определяет простое ли число переданный аргумент
     */
    public static boolean isPrime(int p) {
        boolean prime_flag;
        if(p == 1 || p == 2 || p == 3) return true;
        else if(p == 0 || p % 2 == 0) return false; 
        else {
            prime_flag = true;
            for(int d = 3; d < p; d += 2) {
                if(p % d == 0) {
                    prime_flag = false;
                    break;
                }
            }
        } 
        return prime_flag;
    }

    /**
     * Метод формирует массив простых чисел до n
     */ 
    public static int[] solution(int n) {
        List<Integer> resultList = new ArrayList<>();
        for(int i = 1; i < n; i++) {
            if(isPrime(i)) resultList.add(i);
        }
        int[] result = new int[resultList.size()];
        for(int i = 0; i < result.length; i++) {
            result[i] = (Integer) resultList.get(i);
        }
        return result;
    }
}