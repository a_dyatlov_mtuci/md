/**
 * Главный класс программы Palindrome
 */
class Palindrome{
    public static void main(String[] args) {
        for(int i = 0; i < args.length; i++) {
            String s = args[i];
            if(isPalindrome(s)) {
                System.out.printf("%s - is a palindrome!\n", s);
            }
            else {
                System.out.printf("%s - is not a palindrome.\n", s);
            }
        }
    }
    
    /**
     * Метод, возвращающий перевёрнутую строку
     */ 
    public static String reverseStr(String rawStr) {
        String reversedStr = ""; 
        for(int i = rawStr.length() - 1; i >= 0; i--){
            reversedStr += rawStr.charAt(i);
        }
        return reversedStr;
    }

    /**
     * Метод, определяющий является ли строка аргумент палиндромом 
     */ 
    public static boolean isPalindrome(String s) {
        if(s.equals(reverseStr(s))) {
            return true;
        }
        else return false;
    }
}