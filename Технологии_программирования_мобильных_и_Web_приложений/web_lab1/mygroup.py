# /usr/bin/env python 3
# -*- coding: utf-8 -*-

from typing import Iterable, Union

groupmates = [
    {
        "name": "Александр",
        "surname": "Иванов",
        "exams": ["Информатика", "ЭЭиС", "Web"],
        "marks": [4, 3, 5]
    },
    {
        "name": "Иван",
        "surname": "Петров",
        "exams": ["История", "АиГ", "КТП"],
        "marks": [4, 4, 4]
    },
    {
        "name": "Кирилл",
        "surname": "Смирнов",
        "exams": ["Философия", "ИС", "КТП"],
        "marks": [4, 5, 5]
    },
    {
        "name": "Ирина",
        "surname": "Нежданова",
        "exams": ["Философия", "Информатика", "АиГ"],
        "marks": [5, 3, 5]
    },
    {
        "name": "Василий",
        "surname": "Булатов",
        "exams": ["История", "Философия", "Информатика"],
        "marks": [3, 4, 4]
    }
]


def filter_by_mark(groupmates_: Iterable, mark: Union[int, float]) -> Iterable:
    return sorted(list(filter(lambda x: sum(x.get("marks")) / len(x.get("marks")) >= mark, groupmates_)),
                  key=lambda x: sum(x.get("marks")) / len(x.get("marks")), reverse=True)


def print_students(students: Iterable):
    print(u"Имя".ljust(15), u"Фамилия".ljust(10), u"Экзамены".ljust(30), u"Оценки".ljust(20))
    for student in students:
        print(student["name"].ljust(15),
              student["surname"].ljust(10),
              str(student["exams"]).ljust(30),
              str(student["marks"]).ljust(20))


if __name__ == "__main__":
    print_students(filter_by_mark(groupmates, float(input("Введите минимальную среднюю оценку: "))))
