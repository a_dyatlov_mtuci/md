#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import random
import pulp
from cvxopt.modeling import variable, op
from scipy.optimize import linprog
import numpy as np
import re
import time
from collections import OrderedDict

MAX_W = 10
MAX_C = 25
MIN_SIZE = 5

# Условия закрытой транспортной по умолчанию
default_data = {
    'wh': [400, 100, 120, 220, 310],
    'clients': [150, 60, 72, 116, 230, 180, 94, 15, 103, 48, 82],
    'costs': [
        [8, 7, 10, 4, 11, 4, 8, 8, 10, 6, 6],
        [5, 11, 11, 4, 6, 2, 7, 10, 9, 11, 3],
        [3, 4, 8, 3, 5, 9, 6, 5, 10, 8, 5],
        [11, 5, 6, 8, 7, 12, 5, 5, 8, 5, 10],
        [11, 8, 5, 12, 9, 12, 2, 9, 11, 10, 10]
    ]}


def rand_data() -> dict:
    """
    Задание случайных условий транспортной задачи
    """
    in_data = {}
    warehouse_size = random.randint(MIN_SIZE, MAX_W)
    client_size = random.randint(MIN_SIZE, MAX_C)
    w_volumes = [random.randint(50, 100) for _ in range(warehouse_size)]
    c_volumes = [random.randint(0, 50) for _ in range(client_size)]
    costs = []
    for w in range(warehouse_size):
        costs.append([random.randint(1, 10) for c in range(client_size)])
    in_data.update(wh=w_volumes, clients=c_volumes, costs=costs)
    return in_data


def open2close(conditions) -> dict:
    """
    Преобразование открытой транспортной задачи в закрытую
    """
    disbalance = abs(sum(conditions['wh']) - sum(conditions['clients']))
    # компенсируем дисбаланс
    if sum(conditions['wh']) > sum(conditions['clients']):
        # вводим фиктивного потребителя
        conditions['clients'].append(disbalance)
        for c in conditions['costs']:
            c.append(0)
    else:
        # вводим фиктивного поставщика
        conditions['wh'].append(disbalance)
        conditions['costs'].append([0 for _ in range(len(conditions['clients']))])
    return conditions


def fancy_table(table: dict) -> None:
    """
    Печать данных в виде отформатированной таблицы
    """
    if table.get('clients'):
        print("Условия транспортной задачи")
        print("{:^20s}".format("Поставщики/клиенты"), end='\t')
        for c in table['clients']:
            print(f"{c:3d}", end='\t')
        print('')
    wc = 0
    for w in table['wh']:
        print(f"{w:^20d}", end='\t')
        for cost in table['costs'][wc]:
            print(f"{cost:3d}", end='\t')
        print('')
        wc += 1


def solve_pulp(conditions: dict):
    start = time.time()
    warehouses = [f"wh{i}" for i in range(1, len(conditions['wh']) + 1)]
    clients = [f"client{i}" for i in range(1, len(conditions['clients']) + 1)]
    supplies = dict(zip(warehouses, conditions['wh']))
    demands = dict(zip(clients, conditions['clients']))
    routes = [(w, c) for w in warehouses for c in clients]
    costs_dict = {}
    for w in range(1, len(conditions['costs']) + 1):
        costs_dict[f"wh{w}"] = {}
        for c in range(1, len(conditions['costs'][w - 1]) + 1):
            costs_dict[f"wh{w}"][f"client{c}"] = conditions['costs'][w - 1][c - 1]
    problem = pulp.LpProblem("Transport_problem_by_pulp", pulp.LpMinimize)
    route_vars = pulp.LpVariable.dicts("X", (warehouses, clients), 0, None, pulp.LpInteger)
    problem += pulp.lpSum([route_vars[w][c] * costs_dict[w][c] for (w, c) in routes])

    for w in warehouses:
        problem += pulp.lpSum([route_vars[w][c] for c in clients]) <= supplies[w], f"Сумма поставок со склада {w}"

    for c in clients:
        problem += pulp.lpSum([route_vars[w][c] for w in warehouses]) >= demands[c], f"Сумма требований клиента {c}"

    problem.solve()
    print("Результат решения pulp:")
    ans_data = {}
    for var in problem.variables():
        ans_data.update([(var.name, var.varValue)])
    ordered_data = OrderedDict(sorted(ans_data.items(), key=lambda t: len(t[0])))
    for k,v in ordered_data.items():
        print(f"{k} = {v}", end='\t')
    print('')
    print(f"Стоимость доставки: {pulp.value(problem.objective)}")
    print("Время решения:", time.time() - start)


def solve_cvxopt(conditions: dict):
    start = time.time()
    # вектор стоимости поставок
    flat_costs = [cost for subcosts in conditions['costs'] for cost in subcosts]
    x = variable(len(flat_costs), 'X')
    # целевая функция
    z = sum([x[i] * flat_costs[i] for i in range(len(flat_costs))])
    # ограничения по транспортной задаче
    constraints = []
    for s in range(len(conditions['wh'])):
        constraints.append(sum(x[i] for i in range(s * len(conditions['costs'][s]),
                                                    (s + 1) * len(conditions['costs'][s]))) <=
                                                    conditions['wh'][s])
    for c in range(len(conditions['clients'])):
        constraints.append(sum(x[i] for i in range(c, len(flat_costs), len(conditions['costs'][0]))) ==
                           conditions['clients'][c])
    constraints.append(x >= 0)
    # процесс решения с помощью glpk
    problem = op(z, constraints)
    problem.solve(solver='glpk')
    problem.status
    print("Результат решения cvxopt: ")
    for x_val in x.value:
        print(x_val, end='\t')
    print('')
    print("Стоимость доставки:", problem.objective.value()[0])
    print("Время решения:", time.time() - start)


def solve_optimize(conditions: dict):
    start = time.time()
    # вектор коэффициентов целевой функции
    A = np.array(conditions['costs'])
    m, n = A.shape
    c = list(np.reshape(A, n*m))
    # Матрица ограничений в виде неравенств
    A_ub = np.zeros([m, m*n])
    for i in np.arange(0, m, 1):
        for j in np.arange(0, n * m, 1):
            if i * n <= j <= n + i * n - 1:
                A_ub[i, j] = 1
    # Вектор ограничений по неравенствам
    # b_ub = np.array(conditions['wh'])
    b_ub = conditions['wh']
    # Матрица ограничений в виде равенств
    A_eq = np.zeros([n, m*n])
    for i in np.arange(0, n, 1):
        k = 0
        for j in np.arange(0, n * m, 1):
            if j == k * n + i:
                A_eq[i, j] = 1
                k += 1
    # Вектор ограничений по равенствам
    # b_eq = np.array(conditions['clients'])
    b_eq = conditions['clients']
    # границы значений искомой переменной
    bounds = [(0, None) for _ in range(len(c))]
    problem = linprog(c, A_ub, b_ub, A_eq, b_eq, bounds=bounds, method='revised simplex')
    print("Результат решения optimize: ")
    print(problem)
    print("Время решения:", time.time() - start)


if __name__ == '__main__':
    aparser = argparse.ArgumentParser()
    aparser.add_argument('-r', '--random', default='N',
                         choices=('y', 'Y', 'yes', 'Yes', 'YES', 'no', 'NO', 'No', 'n', 'N'),
                         help='Randomising input data')
    aparser.add_argument('-l', '--lib', default='all', choices=('all', 'pulp', 'cvxopt', 'optimize'),
                         help='choosing optimisation library')
    args = aparser.parse_args()
    conditions = {}
    closed = True
    if re.search(r'[y,Y][es,ES]?', args.random):
        conditions = rand_data()
    else:
        conditions = default_data
    # определение закрытой транспортной задачи
    if sum(conditions['wh']) != sum(conditions['clients']):
        conditions = open2close(conditions)
    fancy_table(conditions)
    if args.lib == 'all':
        for l in ("cvxopt", "pulp", "optimize"):
            eval(f"solve_{l}(conditions)")
    else:
        eval(f"solve_{args.lib}(conditions)")
