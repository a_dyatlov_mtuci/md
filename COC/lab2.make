EXECUTABLE := lab2
override COMPILE_FLAGS := -pipe
SRC := src
BIN := bin
SEARCH := $(addsuffix /*.cpp,$(SRC))

all: $(EXECUTABLE)
	
$(addsuffix /$(EXECUTABLE),$(BIN)): $(notdir $(patsubst %.cpp,%.o,$(wildcard $(SEARCH))))
	echo $(SEARCH)-hello
	g++ $^ $(COMPILE_FLAGS) -o $@

VPATH := $(SRC) $(BIN)

%.o: %.cpp
	g++ -c -MD $(addprefix -I ,$(SRC)) $(COMPILE_FLAGS) $<
include $(wildcard *.d)

clean:
	rm -f $(BIN)/* *.o *.d