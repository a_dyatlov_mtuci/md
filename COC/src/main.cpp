#include <iostream>
#include "lab2.h"

using namespace std;

int main(){
    int n;
    print_hello();
    cout << endl;
    cin >> n;
    cout << "Факториал числа " << n << ": " << factorial(n) << endl;
    return 0;
}