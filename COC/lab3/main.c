#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "thread_ex.h"

pthread_t tid[THREAD_NUM];
int global_counter = 0;

int main(int argc, char *argv[]) {
	char *fname = "/tmp/lab3.file";
	FILE *fp;
	int th_state;
	int th_counter = 0;
	fp = fopen(fname, "w+");
	while (th_counter < (THREAD_NUM)) {
		if (th_counter != (THREAD_NUM - 1)) th_state = pthread_create(&(tid[th_counter]), 
										NULL,
										trywrite,
										(void *)fp);
		else th_state = pthread_create(&(tid[th_counter]), 
						NULL,
						tryread,
						(void *)fp);
		th_counter++;
	}
	for (int th = 0; th < THREAD_NUM; th++) {
		pthread_join(tid[th], NULL);  
	}
	fclose(fp);
	return 0;
}