#include "thread_ex.h"
pthread_mutex_t lock;

void* trywrite(void * arg) {
	pthread_mutex_lock(&lock);
	FILE* fp = (FILE*)arg;
	if (fprintf(fp, "record #%i\n", ++global_counter) != 0) printf("Thread writer #%i\n", global_counter);
	else perror("Writing error");
	pthread_mutex_unlock(&lock);
}

void* tryread(void * arg) {
	pthread_mutex_lock(&lock);
	char buff[SIZE];
	FILE* fp = (FILE*)arg;
	fseek(fp, 0, SEEK_SET);
	fgets(buff, sizeof(buff), fp);
	printf("Thread reader #%i: %s\n", ++global_counter, buff);
	pthread_mutex_unlock(&lock);
}