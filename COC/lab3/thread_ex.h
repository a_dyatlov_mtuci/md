#include <stdio.h>
#include <pthread.h>
#include <errno.h>
#define SIZE 255
#define THREAD_NUM 10

extern pthread_mutex_t lock;
extern int global_counter;
void* trywrite(void * arg);
void* tryread(void * arg);